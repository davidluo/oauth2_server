package com.example.demo

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint

// sample app: http://blog.didispace.com/spring-security-oauth2-xjf-1/
@SpringBootApplication
//@EnableOAuth2Sso
class DemoApplication

fun main(args: Array<String>) {
    System.out.println("Welcome to kotlin world...")
    SpringApplication.run(DemoApplication::class.java, *args)
}
