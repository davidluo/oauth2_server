package com.example.demo.config

import mu.KLogging
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer

@Configuration
@EnableResourceServer
class ResourceServiceConfiguration : ResourceServerConfigurerAdapter(){
    override fun configure(http: HttpSecurity) {
        super.configure(http)
                // session creation to be allowed (it's disabled by default in 2.0.6)
        logger.info("config resource server")
                http
                .authorizeRequests()
                        .antMatchers("/public/**").permitAll()
//                    .antMatchers("/product/**").access("#oauth2.hasScope('select') and hasRole('ROLE_USER')")
                .antMatchers("/demo/**").authenticated();//配置order访问控制，必须认证过后才可以访问
    }

    override fun configure(resources: ResourceServerSecurityConfigurer) {
        super.configure(resources)
        resources.resourceId("demo")
    }

    companion object : KLogging()
}